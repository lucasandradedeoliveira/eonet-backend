namespace EonetAPI.Domain.Entities
{
    public class Category
    {
        public int id { get; set ; }
        public string title { get; set; }
    }
}
