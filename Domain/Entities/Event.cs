using System.Collections.Generic;

namespace EonetAPI.Domain.Entities
{
    public class Event
    {
        public string id { get; set ; }
        public string title { get; set; }
        public string closed { get; set; }
        public List<Category> categories { get; set; }
        public List<Source> sources { get; set; }
        public List<Geometry> geometries { get; set; }
    }
}
