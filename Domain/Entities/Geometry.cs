using System.Collections.Generic;
using System.Runtime.Serialization;

namespace EonetAPI.Domain.Entities
{
    [DataContract(Name="geometries")]
    public class Geometry
    {
        [DataMember(Name="date")]
        public string date { get; set; }
        [DataMember(Name="type")]
        public string type { get; set; }
        [DataMember(Name="coordinates")]
        private List<object> coordinates;

        public object latitude
        {
            get
            {
                if (coordinates.Count > 0) {
                    return coordinates[0];
                }

                return "";
            }
        }

        public object longitude
        {
            get
            {
                if (coordinates.Count > 1) {
                    return coordinates[1];
                }

                return "";
            }
        }
    }
}
