using System.Collections.Generic;
using EonetAPI.Domain.Entities;
using EonetAPI.Domain.ValueObjects;
using System.Threading.Tasks;

namespace EonetAPI.Domain.Repositories
{
    public interface EventRepository
    {
        Task<List<Event>> List(Query query);
        Task<Event> Get(string eventId);
    }
}