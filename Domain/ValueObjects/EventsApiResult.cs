using System.Collections.Generic;
using EonetAPI.Domain.Entities;

namespace EonetAPI.Domain.ValueObjects
{
    public class EventsApiResult
    {
        public List<Event> events { get; set; }
    }
}
