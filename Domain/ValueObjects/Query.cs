using System.Collections.Generic;
using System.Linq;

namespace EonetAPI.Domain.ValueObjects
{
    public class Query
    {
        private readonly string[] allowedOrderList = {"category", "status"};
        public string order { get; set; }
        public int days { get; set; }
        public string status { get; set; }

        public bool HasOrder()
        {
            if (string.IsNullOrEmpty(order)) {
                return false;
            }
            
            return allowedOrderList.Contains(order);
        }
    }
}
