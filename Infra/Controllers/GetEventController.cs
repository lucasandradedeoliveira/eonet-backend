using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EonetAPI.Domain.Entities;
using EonetAPI.Domain.Repositories;

namespace EonetAPI.Infra.Controllers
{
    [ApiController]
    [Route("/events/{eventId}")]
    public class GetEventController : ControllerBase
    {
        private readonly ILogger<GetEventController> _logger;
        private readonly EventRepository repository;

        public GetEventController(ILogger<GetEventController> logger, EventRepository repository)
        {
            _logger = logger;
            this.repository = repository;
        }

        [HttpGet]
        public Event Handle(string eventId)
        {
            try {
                return repository.Get(eventId).Result;
            } catch (Exception e) {
                _logger.LogError(e, "Error fetching event");
            }

            return null;
        }
    }
}
