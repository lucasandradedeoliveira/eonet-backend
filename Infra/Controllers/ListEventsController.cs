using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using EonetAPI.Domain.Entities;
using EonetAPI.Domain.Repositories;
using EonetAPI.Domain.ValueObjects;

namespace EonetAPI.Infra.Controllers
{
    [ApiController]
    [Route("/events")]
    public class ListEventsController : ControllerBase
    {
        private readonly ILogger<ListEventsController> _logger;

        private readonly EventRepository repository;

        public ListEventsController(ILogger<ListEventsController> logger, EventRepository repository)
        {
            _logger = logger;
            this.repository = repository;
        }

        [HttpGet]
        public IEnumerable<Event> Handle([FromQuery] Query query)
        {
            try {
                return repository.List(query).Result;
            } catch (Exception e) {
                _logger.LogError(e, "Error listing events");
            }

            return new List<Event>();
        }
    }
}
