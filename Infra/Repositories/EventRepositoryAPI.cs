using System;
using System.Collections.Generic;
using EonetAPI.Domain.Entities;
using EonetAPI.Domain.Repositories;
using EonetAPI.Domain.ValueObjects;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace EonetAPI.Infra.Repositories
{
    public class EventRepositoryAPI : EventRepository
    {
        private HttpClient client;

        public EventRepositoryAPI(HttpClient client)
        {
            this.client = client;
        }

        private string BuildUrl(Query query)
        {
            string url = "?limit=200&";

            if (query.days > 0) {
                url += "days=" + query.days + "&";
            }

            if (!string.IsNullOrEmpty(query.status)) {
                url += "status=" + query.status;
            }

            return url;
        }

        private List<Event> Sort(List<Event> list, Query query)
        {
            if (query.HasOrder()) {
                if (query.order == "status") {
                    list.Sort((x, y) => 
                    {
                        if (string.IsNullOrEmpty(x.closed)) {
                            return 1;
                        }

                        if (string.IsNullOrEmpty(y.closed)) {
                            return -1;
                        }
                        
                        return x.closed.CompareTo(y.closed);
                    });
                }

                if (query.order == "category") {
                    list.Sort((x, y) => x.categories[0].title.CompareTo(y.categories[0].title));
                }
            }

            return list;
        }

        public async Task<List<Event>> List(Query query)
        {
            var serializer = new DataContractJsonSerializer(typeof(EventsApiResult));
            var streamTask = await client.GetStreamAsync(BuildUrl(query));
            var repositories = serializer.ReadObject(streamTask) as EventsApiResult;
            return Sort(repositories.events, query);
        }

        public async Task<Event> Get(string eventId)
        {
            var serializer = new DataContractJsonSerializer(typeof(Event));
            var streamTask = await client.GetStreamAsync(eventId);
            var result = serializer.ReadObject(streamTask) as Event;
            return result;
        }
    }
}
